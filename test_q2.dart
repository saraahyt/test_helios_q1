void main() {
  String articles = '163841689525773';
  List<int> itemSizes = articles.split('').map(int.parse).toList();

  itemSizes.sort((a, b) => b.compareTo(a));

  List<List<int>> boxes = [];

  for (var item in itemSizes) {
    bool placed = false;

    for (var box in boxes) {
      int currentSum = box.reduce((a, b) => a + b);
      if (currentSum + item <= 10) {
        box.add(item);
        placed = true;
        break;
      }
    }

    if (!placed) {
      boxes.add([item]);
    }
  }

  String packedBoxes = boxes.map((carton) => carton.join()).join('/');
  print('Articles : $articles');
  print('Cartons optimisés : $packedBoxes');
  print('Nombre de cartons utilisés : ${boxes.length}');
}
