import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:test_helios_q1/core/exceptions.dart';
import 'package:test_helios_q1/features/users/models/api_user_model.dart';
import 'package:test_helios_q1/features/users/ui/user_list/users_bloc.dart';
import 'package:test_helios_q1/features/users/ui/user_list/users_event.dart';
import 'package:test_helios_q1/features/users/ui/user_list/users_state.dart';
import 'package:test_helios_q1/features/users/usecases/get_users_usecases.dart';

class MockGetUsers extends Mock implements GetUsersUsecase {}

main() {
  late UsersBloc bloc;
  late GetUsersUsecase mockGetUsers;

  setUp(() {
    mockGetUsers = MockGetUsers();

    bloc = UsersBloc(
      getUsers: mockGetUsers,
    );
  });

  const fakeUser = ApiUser(
    name: ApiName(
      title: "Mr",
      first: "John",
      last: "Doe",
    ),
    gender: "male",
    location: ApiLocation(city: 'Paris', state: 'Test', country: 'France'),
  );

  blocTest(
    'load users produces loaded state',
    build: () {
      when(() => mockGetUsers.call()).thenAnswer((_) async => [fakeUser]);
      return bloc;
    },
    act: (UsersBloc bloc) {
      bloc.add(const UsersEvent.loadUsers());
    },
    expect: () => [
      const UsersState.loading(users: []),
      const UsersState.loaded(
        users: [fakeUser],
      ),
    ],
  );

  blocTest(
    'load users produces loaded state',
    build: () {
      when(() => mockGetUsers.call())
          .thenThrow(HeliosException(message: "Error"));

      return bloc;
    },
    act: (UsersBloc bloc) {
      bloc.add(const UsersEvent.loadUsers());
    },
    expect: () => [
      const UsersState.loading(users: []),
      const UsersState.failure(
        users: [],
        message: 'Error',
      ),
    ],
  );
}
