import 'package:flutter/material.dart';
import 'package:test_helios_q1/features/users/models/api_user_model.dart';

class UserDetailsPage extends StatelessWidget {
  final ApiUser user;

  const UserDetailsPage({
    super.key,
    required this.user,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("${user.name.first} ${user.name.last}"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Nom : ${user.name.first} ${user.name.last}"),
            Text("Genre : ${user.gender}"),
            Text(
                "Localisation : ${user.location.city}, ${user.location.state}, ${user.location.country}"),
          ],
        ),
      ),
    );
  }
}
