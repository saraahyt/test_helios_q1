import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:test_helios_q1/app/service_locator.dart';
import 'package:test_helios_q1/core/exceptions.dart';
import 'package:test_helios_q1/features/users/ui/user_list/users_event.dart';
import 'package:test_helios_q1/features/users/ui/user_list/users_state.dart';
import 'package:test_helios_q1/features/users/usecases/get_users_usecases.dart';

class UsersBloc extends Bloc<UsersEvent, UsersState> {
  late final GetUsersUsecase _getUsers;

  UsersBloc({
    GetUsersUsecase? getUsers,
  }) : super(const UsersState.initial()) {
    _getUsers = getUsers ?? getIt.get();

    on<UsersEventLoadUsers>(_onEventLoadUsers);
  }

  FutureOr<void> _onEventLoadUsers(
      UsersEventLoadUsers event, Emitter<UsersState> emit) async {
    try {
      emit(const UsersState.loading(users: []));
      final result = await _getUsers();

      emit(UsersState.loaded(users: result));
    } on HeliosException catch (e) {
      emit(UsersState.failure(
        users: [],
        message: e.message,
      ));
    }
  }
}
