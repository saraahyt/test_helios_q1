// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'users_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$UsersState {
  List<ApiUser> get users => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<ApiUser> users) initial,
    required TResult Function(List<ApiUser> users) loading,
    required TResult Function(List<ApiUser> users) loaded,
    required TResult Function(List<ApiUser> users, String message) failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(List<ApiUser> users)? initial,
    TResult? Function(List<ApiUser> users)? loading,
    TResult? Function(List<ApiUser> users)? loaded,
    TResult? Function(List<ApiUser> users, String message)? failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<ApiUser> users)? initial,
    TResult Function(List<ApiUser> users)? loading,
    TResult Function(List<ApiUser> users)? loaded,
    TResult Function(List<ApiUser> users, String message)? failure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(UsersStateInitial value) initial,
    required TResult Function(UsersStateLoading value) loading,
    required TResult Function(UsersStateLoaded value) loaded,
    required TResult Function(UsersStateFailure value) failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(UsersStateInitial value)? initial,
    TResult? Function(UsersStateLoading value)? loading,
    TResult? Function(UsersStateLoaded value)? loaded,
    TResult? Function(UsersStateFailure value)? failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(UsersStateInitial value)? initial,
    TResult Function(UsersStateLoading value)? loading,
    TResult Function(UsersStateLoaded value)? loaded,
    TResult Function(UsersStateFailure value)? failure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $UsersStateCopyWith<UsersState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UsersStateCopyWith<$Res> {
  factory $UsersStateCopyWith(
          UsersState value, $Res Function(UsersState) then) =
      _$UsersStateCopyWithImpl<$Res, UsersState>;
  @useResult
  $Res call({List<ApiUser> users});
}

/// @nodoc
class _$UsersStateCopyWithImpl<$Res, $Val extends UsersState>
    implements $UsersStateCopyWith<$Res> {
  _$UsersStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? users = null,
  }) {
    return _then(_value.copyWith(
      users: null == users
          ? _value.users
          : users // ignore: cast_nullable_to_non_nullable
              as List<ApiUser>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$UsersStateInitialImplCopyWith<$Res>
    implements $UsersStateCopyWith<$Res> {
  factory _$$UsersStateInitialImplCopyWith(_$UsersStateInitialImpl value,
          $Res Function(_$UsersStateInitialImpl) then) =
      __$$UsersStateInitialImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({List<ApiUser> users});
}

/// @nodoc
class __$$UsersStateInitialImplCopyWithImpl<$Res>
    extends _$UsersStateCopyWithImpl<$Res, _$UsersStateInitialImpl>
    implements _$$UsersStateInitialImplCopyWith<$Res> {
  __$$UsersStateInitialImplCopyWithImpl(_$UsersStateInitialImpl _value,
      $Res Function(_$UsersStateInitialImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? users = null,
  }) {
    return _then(_$UsersStateInitialImpl(
      users: null == users
          ? _value._users
          : users // ignore: cast_nullable_to_non_nullable
              as List<ApiUser>,
    ));
  }
}

/// @nodoc

class _$UsersStateInitialImpl implements UsersStateInitial {
  const _$UsersStateInitialImpl({final List<ApiUser> users = const []})
      : _users = users;

  final List<ApiUser> _users;
  @override
  @JsonKey()
  List<ApiUser> get users {
    if (_users is EqualUnmodifiableListView) return _users;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_users);
  }

  @override
  String toString() {
    return 'UsersState.initial(users: $users)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$UsersStateInitialImpl &&
            const DeepCollectionEquality().equals(other._users, _users));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_users));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$UsersStateInitialImplCopyWith<_$UsersStateInitialImpl> get copyWith =>
      __$$UsersStateInitialImplCopyWithImpl<_$UsersStateInitialImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<ApiUser> users) initial,
    required TResult Function(List<ApiUser> users) loading,
    required TResult Function(List<ApiUser> users) loaded,
    required TResult Function(List<ApiUser> users, String message) failure,
  }) {
    return initial(users);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(List<ApiUser> users)? initial,
    TResult? Function(List<ApiUser> users)? loading,
    TResult? Function(List<ApiUser> users)? loaded,
    TResult? Function(List<ApiUser> users, String message)? failure,
  }) {
    return initial?.call(users);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<ApiUser> users)? initial,
    TResult Function(List<ApiUser> users)? loading,
    TResult Function(List<ApiUser> users)? loaded,
    TResult Function(List<ApiUser> users, String message)? failure,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(users);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(UsersStateInitial value) initial,
    required TResult Function(UsersStateLoading value) loading,
    required TResult Function(UsersStateLoaded value) loaded,
    required TResult Function(UsersStateFailure value) failure,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(UsersStateInitial value)? initial,
    TResult? Function(UsersStateLoading value)? loading,
    TResult? Function(UsersStateLoaded value)? loaded,
    TResult? Function(UsersStateFailure value)? failure,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(UsersStateInitial value)? initial,
    TResult Function(UsersStateLoading value)? loading,
    TResult Function(UsersStateLoaded value)? loaded,
    TResult Function(UsersStateFailure value)? failure,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class UsersStateInitial implements UsersState {
  const factory UsersStateInitial({final List<ApiUser> users}) =
      _$UsersStateInitialImpl;

  @override
  List<ApiUser> get users;
  @override
  @JsonKey(ignore: true)
  _$$UsersStateInitialImplCopyWith<_$UsersStateInitialImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$UsersStateLoadingImplCopyWith<$Res>
    implements $UsersStateCopyWith<$Res> {
  factory _$$UsersStateLoadingImplCopyWith(_$UsersStateLoadingImpl value,
          $Res Function(_$UsersStateLoadingImpl) then) =
      __$$UsersStateLoadingImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({List<ApiUser> users});
}

/// @nodoc
class __$$UsersStateLoadingImplCopyWithImpl<$Res>
    extends _$UsersStateCopyWithImpl<$Res, _$UsersStateLoadingImpl>
    implements _$$UsersStateLoadingImplCopyWith<$Res> {
  __$$UsersStateLoadingImplCopyWithImpl(_$UsersStateLoadingImpl _value,
      $Res Function(_$UsersStateLoadingImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? users = null,
  }) {
    return _then(_$UsersStateLoadingImpl(
      users: null == users
          ? _value._users
          : users // ignore: cast_nullable_to_non_nullable
              as List<ApiUser>,
    ));
  }
}

/// @nodoc

class _$UsersStateLoadingImpl implements UsersStateLoading {
  const _$UsersStateLoadingImpl({required final List<ApiUser> users})
      : _users = users;

  final List<ApiUser> _users;
  @override
  List<ApiUser> get users {
    if (_users is EqualUnmodifiableListView) return _users;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_users);
  }

  @override
  String toString() {
    return 'UsersState.loading(users: $users)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$UsersStateLoadingImpl &&
            const DeepCollectionEquality().equals(other._users, _users));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_users));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$UsersStateLoadingImplCopyWith<_$UsersStateLoadingImpl> get copyWith =>
      __$$UsersStateLoadingImplCopyWithImpl<_$UsersStateLoadingImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<ApiUser> users) initial,
    required TResult Function(List<ApiUser> users) loading,
    required TResult Function(List<ApiUser> users) loaded,
    required TResult Function(List<ApiUser> users, String message) failure,
  }) {
    return loading(users);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(List<ApiUser> users)? initial,
    TResult? Function(List<ApiUser> users)? loading,
    TResult? Function(List<ApiUser> users)? loaded,
    TResult? Function(List<ApiUser> users, String message)? failure,
  }) {
    return loading?.call(users);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<ApiUser> users)? initial,
    TResult Function(List<ApiUser> users)? loading,
    TResult Function(List<ApiUser> users)? loaded,
    TResult Function(List<ApiUser> users, String message)? failure,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(users);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(UsersStateInitial value) initial,
    required TResult Function(UsersStateLoading value) loading,
    required TResult Function(UsersStateLoaded value) loaded,
    required TResult Function(UsersStateFailure value) failure,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(UsersStateInitial value)? initial,
    TResult? Function(UsersStateLoading value)? loading,
    TResult? Function(UsersStateLoaded value)? loaded,
    TResult? Function(UsersStateFailure value)? failure,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(UsersStateInitial value)? initial,
    TResult Function(UsersStateLoading value)? loading,
    TResult Function(UsersStateLoaded value)? loaded,
    TResult Function(UsersStateFailure value)? failure,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class UsersStateLoading implements UsersState {
  const factory UsersStateLoading({required final List<ApiUser> users}) =
      _$UsersStateLoadingImpl;

  @override
  List<ApiUser> get users;
  @override
  @JsonKey(ignore: true)
  _$$UsersStateLoadingImplCopyWith<_$UsersStateLoadingImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$UsersStateLoadedImplCopyWith<$Res>
    implements $UsersStateCopyWith<$Res> {
  factory _$$UsersStateLoadedImplCopyWith(_$UsersStateLoadedImpl value,
          $Res Function(_$UsersStateLoadedImpl) then) =
      __$$UsersStateLoadedImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({List<ApiUser> users});
}

/// @nodoc
class __$$UsersStateLoadedImplCopyWithImpl<$Res>
    extends _$UsersStateCopyWithImpl<$Res, _$UsersStateLoadedImpl>
    implements _$$UsersStateLoadedImplCopyWith<$Res> {
  __$$UsersStateLoadedImplCopyWithImpl(_$UsersStateLoadedImpl _value,
      $Res Function(_$UsersStateLoadedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? users = null,
  }) {
    return _then(_$UsersStateLoadedImpl(
      users: null == users
          ? _value._users
          : users // ignore: cast_nullable_to_non_nullable
              as List<ApiUser>,
    ));
  }
}

/// @nodoc

class _$UsersStateLoadedImpl implements UsersStateLoaded {
  const _$UsersStateLoadedImpl({required final List<ApiUser> users})
      : _users = users;

  final List<ApiUser> _users;
  @override
  List<ApiUser> get users {
    if (_users is EqualUnmodifiableListView) return _users;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_users);
  }

  @override
  String toString() {
    return 'UsersState.loaded(users: $users)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$UsersStateLoadedImpl &&
            const DeepCollectionEquality().equals(other._users, _users));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_users));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$UsersStateLoadedImplCopyWith<_$UsersStateLoadedImpl> get copyWith =>
      __$$UsersStateLoadedImplCopyWithImpl<_$UsersStateLoadedImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<ApiUser> users) initial,
    required TResult Function(List<ApiUser> users) loading,
    required TResult Function(List<ApiUser> users) loaded,
    required TResult Function(List<ApiUser> users, String message) failure,
  }) {
    return loaded(users);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(List<ApiUser> users)? initial,
    TResult? Function(List<ApiUser> users)? loading,
    TResult? Function(List<ApiUser> users)? loaded,
    TResult? Function(List<ApiUser> users, String message)? failure,
  }) {
    return loaded?.call(users);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<ApiUser> users)? initial,
    TResult Function(List<ApiUser> users)? loading,
    TResult Function(List<ApiUser> users)? loaded,
    TResult Function(List<ApiUser> users, String message)? failure,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded(users);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(UsersStateInitial value) initial,
    required TResult Function(UsersStateLoading value) loading,
    required TResult Function(UsersStateLoaded value) loaded,
    required TResult Function(UsersStateFailure value) failure,
  }) {
    return loaded(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(UsersStateInitial value)? initial,
    TResult? Function(UsersStateLoading value)? loading,
    TResult? Function(UsersStateLoaded value)? loaded,
    TResult? Function(UsersStateFailure value)? failure,
  }) {
    return loaded?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(UsersStateInitial value)? initial,
    TResult Function(UsersStateLoading value)? loading,
    TResult Function(UsersStateLoaded value)? loaded,
    TResult Function(UsersStateFailure value)? failure,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded(this);
    }
    return orElse();
  }
}

abstract class UsersStateLoaded implements UsersState {
  const factory UsersStateLoaded({required final List<ApiUser> users}) =
      _$UsersStateLoadedImpl;

  @override
  List<ApiUser> get users;
  @override
  @JsonKey(ignore: true)
  _$$UsersStateLoadedImplCopyWith<_$UsersStateLoadedImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$UsersStateFailureImplCopyWith<$Res>
    implements $UsersStateCopyWith<$Res> {
  factory _$$UsersStateFailureImplCopyWith(_$UsersStateFailureImpl value,
          $Res Function(_$UsersStateFailureImpl) then) =
      __$$UsersStateFailureImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({List<ApiUser> users, String message});
}

/// @nodoc
class __$$UsersStateFailureImplCopyWithImpl<$Res>
    extends _$UsersStateCopyWithImpl<$Res, _$UsersStateFailureImpl>
    implements _$$UsersStateFailureImplCopyWith<$Res> {
  __$$UsersStateFailureImplCopyWithImpl(_$UsersStateFailureImpl _value,
      $Res Function(_$UsersStateFailureImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? users = null,
    Object? message = null,
  }) {
    return _then(_$UsersStateFailureImpl(
      users: null == users
          ? _value._users
          : users // ignore: cast_nullable_to_non_nullable
              as List<ApiUser>,
      message: null == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$UsersStateFailureImpl implements UsersStateFailure {
  const _$UsersStateFailureImpl(
      {required final List<ApiUser> users, required this.message})
      : _users = users;

  final List<ApiUser> _users;
  @override
  List<ApiUser> get users {
    if (_users is EqualUnmodifiableListView) return _users;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_users);
  }

  @override
  final String message;

  @override
  String toString() {
    return 'UsersState.failure(users: $users, message: $message)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$UsersStateFailureImpl &&
            const DeepCollectionEquality().equals(other._users, _users) &&
            (identical(other.message, message) || other.message == message));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(_users), message);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$UsersStateFailureImplCopyWith<_$UsersStateFailureImpl> get copyWith =>
      __$$UsersStateFailureImplCopyWithImpl<_$UsersStateFailureImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<ApiUser> users) initial,
    required TResult Function(List<ApiUser> users) loading,
    required TResult Function(List<ApiUser> users) loaded,
    required TResult Function(List<ApiUser> users, String message) failure,
  }) {
    return failure(users, message);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(List<ApiUser> users)? initial,
    TResult? Function(List<ApiUser> users)? loading,
    TResult? Function(List<ApiUser> users)? loaded,
    TResult? Function(List<ApiUser> users, String message)? failure,
  }) {
    return failure?.call(users, message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<ApiUser> users)? initial,
    TResult Function(List<ApiUser> users)? loading,
    TResult Function(List<ApiUser> users)? loaded,
    TResult Function(List<ApiUser> users, String message)? failure,
    required TResult orElse(),
  }) {
    if (failure != null) {
      return failure(users, message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(UsersStateInitial value) initial,
    required TResult Function(UsersStateLoading value) loading,
    required TResult Function(UsersStateLoaded value) loaded,
    required TResult Function(UsersStateFailure value) failure,
  }) {
    return failure(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(UsersStateInitial value)? initial,
    TResult? Function(UsersStateLoading value)? loading,
    TResult? Function(UsersStateLoaded value)? loaded,
    TResult? Function(UsersStateFailure value)? failure,
  }) {
    return failure?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(UsersStateInitial value)? initial,
    TResult Function(UsersStateLoading value)? loading,
    TResult Function(UsersStateLoaded value)? loaded,
    TResult Function(UsersStateFailure value)? failure,
    required TResult orElse(),
  }) {
    if (failure != null) {
      return failure(this);
    }
    return orElse();
  }
}

abstract class UsersStateFailure implements UsersState {
  const factory UsersStateFailure(
      {required final List<ApiUser> users,
      required final String message}) = _$UsersStateFailureImpl;

  @override
  List<ApiUser> get users;
  String get message;
  @override
  @JsonKey(ignore: true)
  _$$UsersStateFailureImplCopyWith<_$UsersStateFailureImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
