import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:test_helios_q1/features/users/models/api_user_model.dart';

part 'users_state.freezed.dart';

mixin _StateMixin {
  List<ApiUser> get users;
}

@freezed
class UsersState with _$UsersState, _StateMixin {
  const factory UsersState.initial({
    @Default([]) List<ApiUser> users,
  }) = UsersStateInitial;

  const factory UsersState.loading({
    required List<ApiUser> users,
  }) = UsersStateLoading;

  const factory UsersState.loaded({
    required List<ApiUser> users,
  }) = UsersStateLoaded;

  const factory UsersState.failure({
    required List<ApiUser> users,
    required String message,
  }) = UsersStateFailure;
}
