import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_helios_q1/app/service_locator.dart';
import 'package:test_helios_q1/features/users/models/api_user_model.dart';
import 'package:test_helios_q1/features/users/ui/user_details/ui/user_details_page.dart';
import 'package:test_helios_q1/features/users/ui/user_list/users_bloc.dart';
import 'package:test_helios_q1/features/users/ui/user_list/users_event.dart';
import 'package:test_helios_q1/features/users/ui/user_list/users_state.dart';
import 'package:test_helios_q1/widgets_utils/dialog_helpers.dart';

class UserListPage extends StatefulWidget {
  const UserListPage({super.key});

  @override
  State<UserListPage> createState() => _UserListPageState();
}

class _UserListPageState extends State<UserListPage> {
  final UsersBloc _bloc = getIt.get();

  @override
  void initState() {
    super.initState();
    _bloc.add(const UsersEvent.loadUsers());
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Utilisateurs'),
      ),
      body: SafeArea(
        child: BlocConsumer(
          bloc: _bloc,
          listener: (BuildContext context, UsersState state) {
            state.maybeMap(
              failure: (state) {
                showMessageDialog(
                  context: context,
                  title: "Liste d'utilisateurs",
                  message: state.message,
                );
              },
              orElse: () => {},
            );
          },
          builder: (BuildContext context, UsersState state) {
            return state.maybeMap(
              loaded: (state) => buildLoadedState(state),
              orElse: () => buildLoadingState(),
            );
          },
        ),
      ),
    );
  }

  Widget buildLoadedState(UsersStateLoaded state) {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: state.users.length,
      itemBuilder: (context, index) {
        final ApiUser user = state.users[index];
        return GestureDetector(
          onTap: () {
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) => UserDetailsPage(user: user),
              ),
            );
          },
          child: Container(
            color: Colors.grey.shade300,
            padding: const EdgeInsets.all(10),
            margin: const EdgeInsets.symmetric(vertical: 3),
            child: Column(
              children: [
                Row(
                  children: [
                    Text(
                      "${user.name.first} ${user.name.last} - ${user.gender}",
                      style: const TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Text("${user.location.city} - ${user.location.country}"),
                  ],
                )
              ],
            ),
          ),
        );
      },
    );
  }

  Widget buildLoadingState() {
    return const Center(
      child: CircularProgressIndicator(),
    );
  }
}
