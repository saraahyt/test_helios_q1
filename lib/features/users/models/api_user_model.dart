import 'package:freezed_annotation/freezed_annotation.dart';

part 'api_user_model.g.dart';

@immutable
@JsonSerializable()
class ApiUser {
  final String gender;
  final ApiName name;
  final ApiLocation location;

  const ApiUser({
    required this.gender,
    required this.name,
    required this.location,
  });

  factory ApiUser.fromJson(Map<String, dynamic> json) =>
      _$ApiUserFromJson(json);

  Map<String, dynamic> toJson() => _$ApiUserToJson(this);
}

@immutable
@JsonSerializable()
class ApiName {
  final String title;
  final String first;
  final String last;

  const ApiName({
    required this.title,
    required this.first,
    required this.last,
  });

  factory ApiName.fromJson(Map<String, dynamic> json) =>
      _$ApiNameFromJson(json);

  Map<String, dynamic> toJson() => _$ApiNameToJson(this);
}

@immutable
@JsonSerializable()
class ApiLocation {
  final String city;
  final String state;
  final String country;

  const ApiLocation({
    required this.city,
    required this.state,
    required this.country,
  });

  factory ApiLocation.fromJson(Map<String, dynamic> json) =>
      _$ApiLocationFromJson(json);

  Map<String, dynamic> toJson() => _$ApiLocationToJson(this);
}
