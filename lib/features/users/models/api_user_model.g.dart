// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'api_user_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ApiUser _$ApiUserFromJson(Map<String, dynamic> json) => ApiUser(
      gender: json['gender'] as String,
      name: ApiName.fromJson(json['name'] as Map<String, dynamic>),
      location: ApiLocation.fromJson(json['location'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ApiUserToJson(ApiUser instance) => <String, dynamic>{
      'gender': instance.gender,
      'name': instance.name,
      'location': instance.location,
    };

ApiName _$ApiNameFromJson(Map<String, dynamic> json) => ApiName(
      title: json['title'] as String,
      first: json['first'] as String,
      last: json['last'] as String,
    );

Map<String, dynamic> _$ApiNameToJson(ApiName instance) => <String, dynamic>{
      'title': instance.title,
      'first': instance.first,
      'last': instance.last,
    };

ApiLocation _$ApiLocationFromJson(Map<String, dynamic> json) => ApiLocation(
      city: json['city'] as String,
      state: json['state'] as String,
      country: json['country'] as String,
    );

Map<String, dynamic> _$ApiLocationToJson(ApiLocation instance) =>
    <String, dynamic>{
      'city': instance.city,
      'state': instance.state,
      'country': instance.country,
    };
