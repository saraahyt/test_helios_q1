import 'package:test_helios_q1/app/service_locator.dart';
import 'package:test_helios_q1/core/api/user_service.dart';
import 'package:test_helios_q1/features/users/models/api_user_model.dart';

class GetUsersUsecase {
  late final UserService _userService;

  GetUsersUsecase({
    UserService? userService,
  }) {
    _userService = userService ?? getIt.get();
  }

  Future<List<ApiUser>> call() async {
    try {
      final users = await _userService.getUsers();

      return users;
    } catch (e) {
      rethrow;
    }
  }
}
