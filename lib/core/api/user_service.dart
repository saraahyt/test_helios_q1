import 'package:dio/dio.dart';
import 'package:test_helios_q1/app/service_locator.dart';
import 'package:test_helios_q1/core/exceptions.dart';
import 'package:test_helios_q1/features/users/models/api_user_model.dart';

class UserService {
  late Dio _dio;

  UserService({
    Dio? dio,
  }) {
    _dio = dio ?? getIt.get();
  }

  Future<List<ApiUser>> getUsers() async {
    try {
      final response = await _dio.get(
        "",
        queryParameters: {"inc": "gender,name,nat,location", "results": 20},
      );

      List<ApiUser> apiUsers = [];
      for (var user in response.data["results"]) {
        ApiUser apiUser = ApiUser.fromJson(user);
        apiUsers.add(apiUser);
      }

      return apiUsers;
    } catch (e) {
      throw HeliosException(
          message:
              "Un problème est survenu pendant la recupération de votre compte");
    }
  }
}
