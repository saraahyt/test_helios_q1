class HeliosException implements Exception {
  final String message;

  HeliosException({
    required this.message,
  });
}
