import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:test_helios_q1/config/constants.dart';
import 'package:test_helios_q1/core/api/user_service.dart';
import 'package:test_helios_q1/features/users/ui/user_list/users_bloc.dart';
import 'package:test_helios_q1/features/users/usecases/get_users_usecases.dart';

GetIt getIt = GetIt.instance;

Future<void> setupLocator() async {
  BaseOptions options = BaseOptions(
    baseUrl: Constants.baseUrl,
  );
  getIt.registerLazySingleton<Dio>(() => Dio(options));

  getIt.registerFactory<UserService>(() => UserService());

  getIt.registerFactory<UsersBloc>(() => UsersBloc());
  getIt.registerFactory<GetUsersUsecase>(() => GetUsersUsecase());
}
